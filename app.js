'use strict';
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const ccxt = require ('ccxt');
const exchange = new ccxt.binance ({
  'rateLimit': 10000, // 统一的交易所属性
  'options': {
    'adjustForTimeDifference': true, //特定交易所的属性
  }
})
exchange.options['adjustForTimeDifference'] = false;

(async function () {
  let huobi = new ccxt.huobipro()
  let binance = new ccxt.binance()
  let okcoin = new ccxt.okcoin()
  let okex = new ccxt.okex()
  console.log (huobi.id, await huobi.fetchTicker('BTC/USDT'))
  console.log (binance.id, await  binance.fetchTicker('BTC/USDT'))
  console.log (okcoin.id,  await okcoin.fetchTicker('BTC/USDT'))
  console.log (okex.id, await okex.fetchTicker('BTC/USDT'))
}) ();


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
