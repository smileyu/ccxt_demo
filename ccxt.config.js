module.exports = {
  apps : [{
    name   : "ccxt",
    script : "./bin/www",
    // watch: [
    //   // 监控变化的目录或文件，一旦变化，自动重启 默认false 可选ture或者文件路径
    //   './routes',
    //   './utils/data.json'
    // ],
    ignore_watch: [
      // 忽视这些目录的变化
      'node_modules',
      'logs'
    ],
    out_file: './logs/out.logs', // 普通日志路径
    error_file: './logs/error.logs', // 错误日志路径
    merge_logs: true,
    log_date_format: "YYYY-MM-DD HH:mm:ss Z",
    env: {
      NODE_ENV: "development"
    },
  }]
}
